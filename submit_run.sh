#!/bin/bash
# MOAB/Torque submission script for SciNet GPC 
#
#PBS -l nodes=8:ppn=8,walltime=00:15:00
#PBS -N 2LPTic
  
# DIRECTORY TO RUN - $PBS_O_WORKDIR is directory job was submitted from
cd $PBS_O_WORKDIR

seed=651621
mpirun -np 50 --bynode src/2LPTic ics_minerva.param 1> logfiles/$seed.stdout 2> logfiles/$seed.stderr

